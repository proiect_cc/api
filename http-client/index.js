const axios = require('axios').default;

const Agent = require('agentkeepalive');
const keepAliveAgent = new Agent({
  maxSockets: 100,
  maxFreeSockets: 10,
  timeout: 60000, // active socket keepalive for 60 seconds
  freeSocketTimeout: 30000, // free socket keepalive for 30 seconds
});

const axiosInstance = axios.create({httpAgent: keepAliveAgent});
const sendRequest = async (options) => {
    try {
        // console.log("before sendRequest...")
        const { data } = await axiosInstance(options);
        // console.log("after sendRequest...")
        return data;
    } catch (error) {

        throw error;
    }
}

module.exports = {
    sendRequest
}
